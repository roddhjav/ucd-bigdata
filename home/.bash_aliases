PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\H\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ll='ls -alFh'
alias l='ll -h'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias c='clear'
alias v='vim'
alias du='du -hs'

function up() {
	for nb in $(seq $1); do
		cd ../
	done
}
alias u='up 1'
alias uu='up 2'
alias uuu='up 3'
alias uuuu='up 4'
alias uuuuu='up 5'
alias uuuuuu='up 6'
alias uuuuuuu='up 7'
alias uuuuuuuu='up 8'
alias uuuuuuuuu='up 9'
alias uuuuuuuuuu='up 10'
alias uuuuuuuuuuu='up 11'
