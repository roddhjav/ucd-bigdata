FROM ubuntu:latest
LABEL maintainer="Alexandre Pujol" \
      project="ucd-bigdata"

# Settings
ENV DEBIAN_FRONTEND=noninteractive \
    TERM=xterm

# Install dependencies
RUN apt-get update -y && apt-get -qq -y --no-install-recommends upgrade && \
    apt-get -qq -y --no-install-recommends install \
        nano vim less wget curl default-jdk \
        python python3 python3-defusedxml && \
    apt-get -qy autoremove && \
    apt-get -qq --purge remove -y .\*-doc$ && \
    apt-get clean && \
    rm -rf /usr/share/doc /usr/share/man && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/*

# Configure the root user
COPY --chown=root:root home/* /root/
