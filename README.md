# ucd-bigdata

**Docker image for ucd-big-data.**


## How to Build

This image is built on Github automatically any time any time a commit is
made or merged to the `master` branch. But if you need to build the image on
your own locally, do the following:

  1. `cd` into this directory.
  3. Run: `docker build -t registry.gitlab.com/roddhjav/ucd-bigdata .`

## How to Use

  1. If you did not built it, pull this image from the private registry:
  ```sh
  docker pull registry.gitlab.com/roddhjav/ucd-bigdata
  ```
  3. Start the container in the background:
  ```sh
  docker run -tid --name bigdata --hostname bigdata registry.gitlab.com/roddhjav/ucd-bigdata
  ```
  4. Start a bash inside the container:
  ```sh
  docker exec -it bigdata bash
  ```
